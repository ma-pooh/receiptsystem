package ja.ac.hal.ths30471.receiptsystem;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class MyDb {
	
	// 接続名
	private String ora_url = "jdbc:oracle:thin:@192.168.184.11:1521:orcl3";
	// ユーザID
	private String ora_id = "ora142";
	// パスワード
	private String ora_pass = "ora142";
	
	// 接続情報
	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	//　Oracle接続
	public MyDb() throws Exception{
		con = DriverManager.getConnection(ora_url, ora_id, ora_pass);
	}

	//　SQL文読み込み
	public void prepare(String sql) throws SQLException {
		ps = con.prepareStatement(sql);
	}
	
	
	
	//　実行
	public void execute() throws SQLException {
		rs = ps.executeQuery();
	}
	public void setInt(int index, int value) throws Exception{
		ps.setInt(index, value);
	}
	public void setString(int index, String value) throws Exception{
		ps.setString(index, value);
	}
	public void executeUpdate() throws Exception{
		ps.executeUpdate();
	}

	
	
	
	//　データ取得
	public boolean next() throws Exception{
		return rs.next();
	}
	public int getInt(String colName) throws Exception{
		return rs.getInt(colName);
	}
	public int getInt(int index) throws Exception{
		return rs.getInt(index);
	}
	public String getString(String colName) throws Exception{
		return rs.getString(colName);
	}
	public void setDate(int index, Date date) throws Exception{
		// ps.setDate(index, date);
		Timestamp t = new Timestamp(date.getTime());
		ps.setTimestamp(index, t);
	}
	
	// 親レシートID取得メソッド
	public int getSequenceValue(String seqName) throws Exception{
			
			this.prepare("SELECT " + seqName + ".CURRVAL FROM DUAL");
			this.execute();
			
			if(this.next()){
					return this.getInt(1);
				}
				else{
					return -1;
				}
		}
}
