package jp.ac.hal.companies;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// 会社一覧
@WebServlet("/companies/index")
public class IndexServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		req.setCharacterEncoding("utf-8");

		String url = "/WEB-INF/jsp/companies/index.jsp";
		try {
			CompaniesDao dao = new CompaniesDao();
			ArrayList<Company> list = null;
			list = dao.select();
			req.setAttribute("list", list);
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		req.getRequestDispatcher(url).forward(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String url = "/WEB-INF/jsp/companies/index.jsp";
		try {
			CompaniesDao dao = new CompaniesDao();
			ArrayList<Company> list = dao.select();
			req.setAttribute("list", list);
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		req.getRequestDispatcher(url).forward(req, resp);
	}
}
