package jp.ac.hal.companies;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/companies/add")
public class AddServlet extends HttpServlet {
	// Postパラメータを受け取る
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		req.setCharacterEncoding("utf-8");
		String name = req.getParameter("name") + req.getParameter("hoge");
		String mail = req.getParameter("mail");

		String url = "/WEB-INF/jsp/companies/index.jsp";
		try {
			CompaniesDao dao = new CompaniesDao();

			// 新規挿入
			Company r = new Company();
			r.setName(name);
			r.setMail(mail);
			dao.insert(r);

			// リダイレクト
			resp.sendRedirect("/ReceiptSystem/companies/index");
			return;

		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		req.getRequestDispatcher(url).forward(req, resp);
	}

	// 新規フォーム表示
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String url = "/WEB-INF/jsp/companies/add.jsp";
		req.getRequestDispatcher(url).forward(req, resp);
	}
}
