package jp.ac.hal.companies;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/companies/edit")
public class EditServlet extends HttpServlet {
	// Postパラメータを受け取る
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		req.setCharacterEncoding("utf-8");
		int id = Integer.parseInt(req.getParameter("id"));
		String name = req.getParameter("name");
		String mail = req.getParameter("mail");

		String url = "/WEB-INF/jsp/companies/index.jsp";
		try {
			CompaniesDao dao = new CompaniesDao();

			// 更新
			Company r = new Company();
			r.setId(id);
			r.setName(name);
			r.setMail(mail);
			dao.update(r);

			// リダイレクト
			resp.sendRedirect("/ReceiptSystem/companies/index");
			return;

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		req.getRequestDispatcher(url).forward(req, resp);
	}

	// 編集フォーム表示
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String url = "/WEB-INF/jsp/companies/edit.jsp";
		try {
			CompaniesDao dao = new CompaniesDao();
			Company company = dao.findById(Integer.parseInt(req.getParameter("id")));
			req.setAttribute("company", company);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		req.getRequestDispatcher(url).forward(req, resp);
	}
}
