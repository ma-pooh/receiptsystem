package jp.ac.hal.companies;

import java.util.ArrayList;

import jp.ac.hal.MyDb;

/**
 * Regionsテーブルにアクセスするためのクラス
 * @author hal
 *
 */
public class CompaniesDao {
	/**
	 * コンストラクタ
	 *
	 * @throws ClassNotFoundException
	 *             ドライバのロードに失敗した場合に発生する
	 */
	public CompaniesDao() throws ClassNotFoundException {
		// OracleDriverをメモリにロード
		Class.forName("oracle.jdbc.driver.OracleDriver");
	}

	// IDで検索
	public Company findById(int id){
		try {
			MyDb db = new MyDb();

			// SQL実行オブジェクトの生成
			db.prepare("select id, name, mail from companies where id = ?");
			db.setInt(1, id);
			db.execute();

			// 取得した件数分オブジェクトを生成
			if (db.next()) {
				// Regionオブジェクトを生成
				Company r = new Company();

				// データを埋める
				r.setId(db.getInt("id"));
				r.setName(db.getString("name"));
				r.setMail(db.getString("mail"));

				// 結果
				return r;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 検索を行う
	 *
	 * @return 検索結果
	 */
	public ArrayList<Company> select() {
		// 結果を入れるリスト
		ArrayList<Company> list = new ArrayList<Company>();

		try {
			MyDb db = new MyDb();

			// SQL実行
			db.prepare("select id, name, mail from companies order by id");
			db.execute();

			// 取得した件数分オブジェクトを生成
			while (db.next()) {
				// Regionオブジェクトを生成
				Company r = new Company();

				// データを埋める
				r.setId(db.getInt("id"));
				r.setName(db.getString("name"));
				r.setMail(db.getString("mail"));

				// リストに追加する
				list.add(r);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	// insert
	public void insert(Company region) {
		try {
			MyDb db = new MyDb();
			db.prepare("insert into companies(id, name, mail) values(companies_seq.nextval, ?, ?)");
			db.setString(1, region.getName());
			db.setString(2, region.getMail());
			db.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * 削除メソッド
	 * @param id 削除対象のid
	 * @return 成功の場合true, 失敗の場合false
	 */
	public boolean delete(int id) {
		// 成功フラグ
		boolean isSuccess = false;

		try {
			// SQL実行
			MyDb db = new MyDb();
			db.prepare("delete from companies where id = ?");
			db.setInt(1, id);
			db.executeUpdate();
			// tryの最後まできたら処理成功
			isSuccess = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		// 成否フラグを返す
		return isSuccess;
	}

	/**
	 * 更新メソッド
	 * @param id 更新対象のid
	 * @return 成功の場合true, 失敗の場合false
	 */
	public boolean incrementGood(int id) {
		// 成功フラグ
		boolean isSuccess = false;

		try {
			// SQL実行
			MyDb db = new MyDb();
			db.prepare("update companies set good = good + 1 where id = ?");
			db.setInt(1, id);
			db.executeUpdate();
			// tryの最後まできたら処理成功
			isSuccess = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		// 成否フラグを返す
		return isSuccess;
	}

	/**
	 * 更新メソッド
	 * @param id 更新対象のid
	 * @return 成功の場合true, 失敗の場合false
	 */
	public boolean update(Company company) {
		try {
			// SQL実行
			MyDb db = new MyDb();
			db.prepare("update companies set name = ?, mail = ? where id = ?");
			db.setString(1, company.getName());
			db.setString(2, company.getMail());
			db.setInt(3, company.getId());
			db.executeUpdate();
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
