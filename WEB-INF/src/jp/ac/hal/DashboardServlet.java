package jp.ac.hal;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/top")
public class DashboardServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.sendRedirect("/ReceiptSystem/top");
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String url = "/WEB-INF/jsp/dashboard.jsp";
		/*
		try {
			// RegionsDAO dao = new RegionsDAO();
			// ArrayList<Region> list = dao.select();
			// req.setAttribute("list", list);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		*/
		req.getRequestDispatcher(url).forward(req, resp);
	}
}
