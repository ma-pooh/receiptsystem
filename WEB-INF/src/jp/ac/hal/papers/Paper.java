package jp.ac.hal.papers;

import java.util.ArrayList;

public class Paper {
	private int id;
	private String name;
	private int company_id;
	private ArrayList<PaperItem> paper_items = new ArrayList<PaperItem>();

	// joinによる取得
	private String company_name;

	// 合計計算用メソッド
	public int calcTotalPrice(){
		int sum = 0;
		for(PaperItem paperItem : paper_items){
			sum += paperItem.calcTotalPrice();
		}
		return sum;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getCompanyId(){
		return company_id;
	}
	public void setCompanyId(int company_id){
		this.company_id = company_id;
	}

	// 明細
	public void setPaperItems(String[] item_ids, String[] item_counts){
		paper_items.clear();
		if(item_ids != null){
			for(int i = 0; i < item_ids.length; i++){
				try{
					int item_id = Integer.parseInt(item_ids[i]);
					int count = Integer.parseInt(item_counts[i]);
					PaperItem paper_item = new PaperItem(this.id, item_id, count);
					paper_items.add(paper_item);
				}
				catch(Exception ex){
				}
			}
		}
	}
	public void addPaperItem(PaperItem paperItem){
		paper_items.add(paperItem);
	}
	public ArrayList<PaperItem> getPaperItems(){
		return this.paper_items;
	}

	// join周り
	public String getCompanyName() {
		return company_name;
	}
	public void setCompanyName(String company_name) {
		this.company_name = company_name;
	}
}
