package jp.ac.hal.papers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.ac.hal.companies.CompaniesDao;
import jp.ac.hal.companies.Company;
import jp.ac.hal.items.Item;
import jp.ac.hal.items.ItemsDao;

@WebServlet("/papers/view")
public class ViewServlet extends HttpServlet {
	// 編集フォーム表示
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String url = "/WEB-INF/jsp/papers/view.jsp";
		try {
			PapersDao dao = new PapersDao();
			Paper paper = dao.findById(Integer.parseInt(req.getParameter("id")));
			req.setAttribute("paper", paper);

			// 会社一覧表示用
			CompaniesDao companiesDao = new CompaniesDao();
			ArrayList<Company> companies = companiesDao.select();
			req.setAttribute("companies", companies);

			// 商品表示用
			ItemsDao itemsDao = new ItemsDao();
			ArrayList<Item> items = itemsDao.select();
			req.setAttribute("items", items);
		} catch (Exception e) {
			e.printStackTrace();
		}
		req.getRequestDispatcher(url).forward(req, resp);
	}
}
