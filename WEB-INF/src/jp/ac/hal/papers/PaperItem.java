package jp.ac.hal.papers;

public class PaperItem{
	public int id;
	public int paper_id;
	public int item_id;
	public int cnt; // 個数
	public String item_name = "";
	public int item_price = 0; // 単価

	// 金額計算
	public int calcTotalPrice(){
		return cnt * item_price;
	}

	public PaperItem(int id, int paper_id, int item_id, int cnt, String item_name, int item_price){
		this.id = id;
		this.paper_id = paper_id;
		this.item_id = item_id;
		this.cnt = cnt;
		this.item_name = item_name;
		this.item_price = item_price;
	}
	public PaperItem(int paper_id, int item_id, int cnt){
		this.id = 0;
		this.paper_id = paper_id;
		this.item_id = item_id;
		this.cnt = cnt;
	}
}
