package jp.ac.hal.papers;

import java.util.ArrayList;

import jp.ac.hal.MyDb;

/**
 * Papersテーブルにアクセスするためのクラス
 * @author hal
 *
 */
public class PapersDao {
	/**
	 * コンストラクタ
	 *
	 * @throws ClassNotFoundException
	 *             ドライバのロードに失敗した場合に発生する
	 */
	public PapersDao() throws ClassNotFoundException {
		// OracleDriverをメモリにロード
		Class.forName("oracle.jdbc.driver.OracleDriver");
	}

	// IDで検索
	public Paper findById(int id){
		try {
			// SQL実行オブジェクトの生成
			MyDb db = new MyDb();
			db.prepare("select papers.id id, papers.name paper_name, company_id, companies.name company_name from papers left join companies on papers.company_id = companies.id where papers.id = ?");
			db.setInt(1, id);
			db.execute();

			// 1件分のオブジェクトを生成
			if (db.next()) {
				// Paperオブジェクトを生成
				Paper paper = new Paper();

				// データを埋める
				paper.setId(db.getInt("id"));
				paper.setName(db.getString("paper_name"));
				paper.setCompanyId(db.getInt("company_id"));

				// join周り
				paper.setCompanyName(db.getString("company_name"));

				// 明細を取得
				db.prepare("select paper_items.id id, paper_id, item_id, cnt, items.name item_name, items.price item_price from paper_items left join items on items.id = item_id where paper_id = ? order by paper_items.id");
				db.setInt(1, id);
				db.execute();
				while(db.next()){
					PaperItem paperItem = new PaperItem(
						db.getInt("id"),
						db.getInt("paper_id"),
						db.getInt("item_id"),
						db.getInt("cnt"),
						db.getString("item_name"),
						db.getInt("item_price")
					);
					paper.addPaperItem(paperItem);
				}

				// 結果
				return paper;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 検索を行う
	 *
	 * @return 検索結果
	 */
	public ArrayList<Paper> select() {
		// 結果を入れるリスト
		ArrayList<Paper> list = new ArrayList<Paper>();

		try {
			MyDb db = new MyDb();

			// SQL実行
			db.prepare("select id, name, company_id from papers order by id");
			db.execute();

			// 取得した件数分オブジェクトを生成
			while (db.next()) {
				// Paperオブジェクトを生成
				Paper r = new Paper();

				// データを埋める
				r.setId(db.getInt("id"));
				r.setName(db.getString("name"));
				r.setCompanyId(db.getInt("company_id"));

				// リストに追加する
				list.add(r);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	// insert
	public void insert(Paper paper) {
		try {
			MyDb db = new MyDb();
			// 発注書
			db.prepare("insert into papers(id, name, company_id) values(papers_seq.nextval, ?, ?)");
			db.setString(1, paper.getName());
			db.setInt(2, paper.getCompanyId());
			db.executeUpdate();

			// 生成されたID
			int newPaperId = db.getSequenceValue("papers_seq");
			paper.setId(newPaperId);

			// 発注書明細
			ArrayList<PaperItem> paper_items = paper.getPaperItems();
			for(PaperItem paper_item : paper_items){
				paper_item.paper_id = newPaperId;
				db.prepare("insert into paper_items(id, paper_id, item_id, cnt) "
							+ "values(paper_items_seq.nextval, ?, ?, ?)");
				db.setInt(1, paper_item.paper_id);
				db.setInt(2, paper_item.item_id);
				db.setInt(3, paper_item.cnt);
				db.executeUpdate();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 更新メソッド
	 * @param id 更新対象のid
	 * @return 成功の場合true, 失敗の場合false
	 */
	public boolean update(Paper paper) {
		try {
			// SQL実行
			MyDb db = new MyDb();
			db.prepare("update papers set name = ?, company_id = ? where id = ?");
			db.setString(1, paper.getName());
			db.setInt(2, paper.getCompanyId());
			db.setInt(3, paper.getId());
			db.executeUpdate();

			// 明細一旦リセット
			db.prepare("delete from paper_items where paper_id = ?");
			db.setInt(1, paper.getId());
			db.executeUpdate();

			// 明細追加しなおし
			ArrayList<PaperItem> paper_items = paper.getPaperItems();
			for(PaperItem paper_item : paper_items){
				paper_item.paper_id = paper.getId();
				db.prepare("insert into paper_items(id, paper_id, item_id, cnt) "
							+ "values(paper_items_seq.nextval, ?, ?, ?)");
				db.setInt(1, paper_item.paper_id);
				db.setInt(2, paper_item.item_id);
				db.setInt(3, paper_item.cnt);
				db.executeUpdate();
			}

			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}


	/**
	 * 削除メソッド
	 * @param id 削除対象のid
	 * @return 成功の場合true, 失敗の場合false
	 */
	public boolean delete(int id) {
		// 成功フラグ
		boolean isSuccess = false;

		try {
			// SQL実行
			MyDb db = new MyDb();
			db.prepare("delete from papers where id = ?");
			db.setInt(1, id);
			db.executeUpdate();
			// tryの最後まできたら処理成功
			isSuccess = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		// 成否フラグを返す
		return isSuccess;
	}
}
