package jp.ac.hal.papers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.ac.hal.companies.CompaniesDao;
import jp.ac.hal.companies.Company;
import jp.ac.hal.items.Item;
import jp.ac.hal.items.ItemsDao;

@WebServlet("/papers/add")
public class AddServlet extends HttpServlet {
	// Postパラメータを受け取る
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		req.setCharacterEncoding("utf-8");
		String name = req.getParameter("name");
		int company_id = Integer.valueOf(req.getParameter("company_id"));
		String[] item_ids = req.getParameterValues("item_ids");
		String[] item_cnts = req.getParameterValues("item_cnts");

		String url = "/WEB-INF/jsp/papers/index.jsp";
		try {
			PapersDao dao = new PapersDao();

			// 新規挿入
			Paper r = new Paper();
			r.setName(name);
			r.setCompanyId(company_id);
			r.setPaperItems(item_ids, item_cnts);
			dao.insert(r);

			// リダイレクト
			resp.sendRedirect("/ReceiptSystem/papers/index");
			return;

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		req.getRequestDispatcher(url).forward(req, resp);
	}

	// 新規フォーム表示
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String url = "/WEB-INF/jsp/papers/add.jsp";
		try {
			// 会社一覧選択用
			CompaniesDao companiesDao = new CompaniesDao();
			ArrayList<Company> companies = companiesDao.select();
			req.setAttribute("companies", companies);

			// 商品選択用
			ItemsDao itemsDao = new ItemsDao();
			ArrayList<Item> items = itemsDao.select();
			req.setAttribute("items", items);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		req.getRequestDispatcher(url).forward(req, resp);
	}
}
