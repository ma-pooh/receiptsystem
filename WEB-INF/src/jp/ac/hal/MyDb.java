package jp.ac.hal;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MyDb {
	// 接続名
	private String ora_url = "jdbc:oracle:thin:@192.168.184.11:1521:orcl3";
	// ユーザID
	private String ora_id = "ora140";
	// パスワード
	private String ora_pass = "ora140";

	// 接続情報
	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	public MyDb() throws Exception{
		con = DriverManager.getConnection(ora_url, ora_id, ora_pass);
	}

	public Connection getCon(){
		return con;
	}

	// 実行
	public void prepare(String sql) throws Exception{
		ps = con.prepareStatement(sql);
	}
	public void setInt(int index, int value) throws Exception{
		ps.setInt(index, value);
	}
	public void setString(int index, String value) throws Exception{
		ps.setString(index, value);
	}
	public void execute() throws Exception{
		rs = ps.executeQuery();
	}
	public void executeUpdate() throws Exception{
		ps.executeUpdate();

	}
	public int getSequenceValue(String seqName) throws Exception{
		this.prepare("SELECT " + seqName + ".CURRVAL FROM DUAL");
		this.execute();
		if(this.next()){
			return this.getInt(1);
		}
		else{
			return -1;
		}
	}
	/*
	public int executeInsert() throws Exception{
		int updatedCount = ps.executeUpdate();
		ResultSet result = ps.getGeneratedKeys();
		result.next();
		int newId = result.getInt(1);
		return newId;
	}
	*/

	// 取得
	public boolean next() throws Exception{
		return rs.next();
	}
	public int getInt(String colName) throws Exception{
		return rs.getInt(colName);
	}
	public int getInt(int index) throws Exception{
		return rs.getInt(index);
	}
	public String getString(String colName) throws Exception{
		return rs.getString(colName);

	}
	public Date getDate(String colName) throws Exception{
		return rs.getDate(colName);
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();

		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (ps != null) {
			try {
				ps.close();
				ps = null;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (con != null) {
			try {
				con.close();
				con = null;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
