package jp.ac.hal.items;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/items/add")
public class AddServlet extends HttpServlet {
	// Postパラメータを受け取る
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		req.setCharacterEncoding("utf-8");
		String name = req.getParameter("name"); // + req.getParameter("hoge");
		int price = Integer.valueOf(req.getParameter("price"));

		String url = "/WEB-INF/jsp/items/index.jsp";
		try {
			ItemsDao dao = new ItemsDao();

			// 新規挿入
			Item r = new Item();
			r.setName(name);
			r.setPrice(price);
			dao.insert(r);

			// リダイレクト
			resp.sendRedirect("/ReceiptSystem/items/index");
			return;

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		req.getRequestDispatcher(url).forward(req, resp);
	}

	// 新規フォーム表示
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String url = "/WEB-INF/jsp/items/add.jsp";
		req.getRequestDispatcher(url).forward(req, resp);
	}
}
