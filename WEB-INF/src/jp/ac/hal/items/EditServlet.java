package jp.ac.hal.items;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/items/edit")
public class EditServlet extends HttpServlet {
	// Postパラメータを受け取る
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		req.setCharacterEncoding("utf-8");
		int id = Integer.parseInt(req.getParameter("id"));
		String name = req.getParameter("name");
		int price = Integer.parseInt(req.getParameter("price"));

		String url = "/WEB-INF/jsp/items/index.jsp";
		try {
			ItemsDao dao = new ItemsDao();

			// 更新
			Item r = new Item();
			r.setId(id);
			r.setName(name);
			r.setPrice(price);
			dao.update(r);

			// リダイレクト
			resp.sendRedirect("/ReceiptSystem/items/index");
			return;

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		req.getRequestDispatcher(url).forward(req, resp);
	}

	// 編集フォーム表示
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String url = "/WEB-INF/jsp/items/edit.jsp";
		try {
			ItemsDao dao = new ItemsDao();
			Item item = dao.findById(Integer.parseInt(req.getParameter("id")));
			req.setAttribute("item", item);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		req.getRequestDispatcher(url).forward(req, resp);
	}
}
