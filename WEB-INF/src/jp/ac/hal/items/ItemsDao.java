package jp.ac.hal.items;

import java.util.ArrayList;

import jp.ac.hal.MyDb;

/**
 * Regionsテーブルにアクセスするためのクラス
 * @author hal
 *
 */
public class ItemsDao {
	/**
	 * コンストラクタ
	 *
	 * @throws ClassNotFoundException
	 *             ドライバのロードに失敗した場合に発生する
	 */
	public ItemsDao() throws ClassNotFoundException {
		// OracleDriverをメモリにロード
		Class.forName("oracle.jdbc.driver.OracleDriver");
	}

	// IDで検索
	public Item findById(int id){
		try {
			// SQL実行オブジェクトの生成
			MyDb db = new MyDb();
			db.prepare("select id, name, price from items where id = ?");
			db.setInt(1, id);
			db.execute();

			// 取得した件数分オブジェクトを生成
			if (db.next()) {
				// Regionオブジェクトを生成
				Item r = new Item();

				// データを埋める
				r.setId(db.getInt("id"));
				r.setName(db.getString("name"));
				r.setPrice(db.getInt("price"));

				// 結果
				return r;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 検索を行う
	 *
	 * @return 検索結果
	 */
	public ArrayList<Item> select() {
		// 結果を入れるリスト
		ArrayList<Item> list = new ArrayList<Item>();

		try {
			MyDb db = new MyDb();

			// SQL実行
			db.prepare("select id, name, price from items order by id");
			db.execute();

			// 取得した件数分オブジェクトを生成
			while (db.next()) {
				// Regionオブジェクトを生成
				Item r = new Item();

				// データを埋める
				r.setId(db.getInt("id"));
				r.setName(db.getString("name"));
				r.setPrice(db.getInt("price"));

				// リストに追加する
				list.add(r);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	// insert
	public void insert(Item region) {
		try {
			MyDb db = new MyDb();
			db.prepare("insert into items(id, name, price) values(items_seq.nextval, ?, ?)");
			db.setString(1, region.getName());
			db.setInt(2, region.getPrice());
			db.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * 削除メソッド
	 * @param id 削除対象のid
	 * @return 成功の場合true, 失敗の場合false
	 */
	public boolean delete(int id) {
		// 成功フラグ
		boolean isSuccess = false;

		try {
			// SQL実行
			MyDb db = new MyDb();
			db.prepare("delete from items where id = ?");
			db.setInt(1, id);
			db.executeUpdate();
			// tryの最後まできたら処理成功
			isSuccess = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		// 成否フラグを返す
		return isSuccess;
	}



	/**
	 * 更新メソッド
	 * @param id 更新対象のid
	 * @return 成功の場合true, 失敗の場合false
	 */
	public boolean update(Item item) {
		try {
			// SQL実行
			MyDb db = new MyDb();
			db.prepare("update items set name = ?, price = ? where id = ?");
			db.setString(1, item.getName());
			db.setInt(2, item.getPrice());
			db.setInt(3, item.getId());
			db.executeUpdate();
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
