package jp.ac.hal.items;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/items/delete")
public class DeleteServlet extends HttpServlet {
	// Postパラメータを受け取る
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		req.setCharacterEncoding("utf-8");
		int id = Integer.parseInt(req.getParameter("id"));

		String url = "/WEB-INF/jsp/items/index.jsp";
		try {
			ItemsDao dao = new ItemsDao();

			// 削除
			dao.delete(id);

			// リダイレクト
			resp.sendRedirect("/ReceiptSystem/items/index");
			return;

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		req.getRequestDispatcher(url).forward(req, resp);
	}

	// ここは使わない
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.sendRedirect("/ReceiptSystem/index");
	}
}
