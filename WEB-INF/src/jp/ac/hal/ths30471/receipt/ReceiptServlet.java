package jp.ac.hal.ths30471.receipt;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.ac.hal.ths30471.items.ItemsDAO;
import jp.ac.hal.ths30471.items.Product;
import jp.ac.hal.ths30471.register.Receipt;

@WebServlet("/receipt")
public class ReceiptServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String url = "/WEB-INF/jsp/receiptsystem/receipt/receipt.jsp";

			ReceiptDAO dao = new ReceiptDAO();
			ArrayList<Receipt> list = null;
			list = dao.select();
			req.setAttribute("list", list);
		
		req.getRequestDispatcher(url).forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

	}
	
	

}
