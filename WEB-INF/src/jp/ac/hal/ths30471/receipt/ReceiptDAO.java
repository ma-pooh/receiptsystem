package jp.ac.hal.ths30471.receipt;

import ja.ac.hal.ths30471.receiptsystem.MyDb;

import java.util.ArrayList;

import jp.ac.hal.ths30471.items.Product;
import jp.ac.hal.ths30471.register.Receipt;

public class ReceiptDAO {
	
	
	// select
	public ArrayList<Receipt> select(){
		
		ArrayList<Receipt> list = new ArrayList<Receipt>();
		
		try {
			
			MyDb db = new MyDb();
			
			db.prepare("SELECT id, created FROM receipt1 ORDER BY id");
			db.execute();
			
			while (db.next()) {
				
				Receipt r = new Receipt();
				
				r.setId(db.getInt("id"));
				
				java.util.Date d = new java.util.Date();
				java.sql.Date sqlD = new java.sql.Date(d.getTime());
				r.setDate(sqlD);
					
				list.add(r);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	// delete
	public boolean delete(int id) {
		// 成功フラグ
		boolean isSuccess = false;

		try {
			// SQL実行
			MyDb db = new MyDb();
			db.prepare("DELETE FROM receipt1 WHERE id = ?");
			db.setInt(1, id);
			db.executeUpdate();
			// tryの最後まできたら処理成功
			isSuccess = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		// 成否フラグを返す
		return isSuccess;
	}
	

}
