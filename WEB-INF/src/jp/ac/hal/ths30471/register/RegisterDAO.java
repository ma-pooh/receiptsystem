package jp.ac.hal.ths30471.register;

import java.util.ArrayList;

import ja.ac.hal.ths30471.receiptsystem.MyDb;

public class RegisterDAO {	
	
	// insert
	public void insert(Receipt r) {
		
		
		try {
			MyDb db = new MyDb();
			
			db.prepare("INSERT INTO receipt1(id, created) VALUES( receipt_seq.nextval, ?)");
			db.setDate(1, r.getDate());
			db.executeUpdate();
			
			int newReceiptId = db.getSequenceValue("receipt_seq");
			
			r.setRp_id(newReceiptId);
			
			ArrayList<ReceiptItem> items = r.items;
			for(ReceiptItem item : items){
				item.rpt_id = r.Rp_id;
				db.prepare("INSERT INTO receipt2(id, receipt1_id, item_id) VALUES(receipt2_seq.nextval, ?, ?)");
				db.setInt(1, item.rpt_id);
				db.setInt(2, item.item_id);
				db.executeUpdate();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
