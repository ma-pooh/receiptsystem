package jp.ac.hal.ths30471.register;

public class ReceiptItem {
	
	public int id;
	public int rpt_id;
	public int item_id;
	
	public String item_name;
	public int item_price;
		
	public ReceiptItem(int rpt_id, int item_id, String item_name, int item_price){
		this.id = 0;
		this.rpt_id = rpt_id;
		this.item_id = item_id;
		this.item_name = item_name;
		this.item_price = item_price;
	}
	
	
	public ReceiptItem(int rpt_id, int item_id){
		this.id = 0;
		this.rpt_id = rpt_id;
		this.item_id = item_id;
	}
	
}
