package jp.ac.hal.ths30471.register;

import java.awt.TrayIcon;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.ac.hal.ths30471.items.ItemsDAO;
import jp.ac.hal.ths30471.items.Product;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String url = "/WEB-INF/jsp/receiptsystem/register/register.jsp";

		try {
			
			ItemsDAO dao = new ItemsDAO();
			ArrayList<Product> list = null;
			list = dao.select();
			req.setAttribute("list", list);
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		req.getRequestDispatcher(url).forward(req, resp);
		
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		req.setCharacterEncoding("utf-8");
		String[] item_ids = req.getParameterValues("item_ids");
		
		String url = "/WEB-INF/jsp/receiptsystem/register/register.jsp";

		
		try {
			
			RegisterDAO dao = new RegisterDAO();
			
			Receipt r = new Receipt();
			
			// 親側の設定
			java.util.Date d = new java.util.Date();
			java.sql.Date sqlD = new java.sql.Date(d.getTime());
			r.setDate(sqlD);
			
			// 子供側の設定
			r.setItemID(item_ids);
			dao.insert(r);
			
			resp.sendRedirect("/ReceiptSystem/register");
			return;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
