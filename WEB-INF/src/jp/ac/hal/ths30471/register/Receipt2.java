package jp.ac.hal.ths30471.register;

import com.sun.xml.internal.bind.v2.model.core.ID;

public class Receipt2 {
	
	private int id;
	private int rp_id;
	private int item_id;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	public int getRp_id() {
		return rp_id;
	}
	public void setRp_id(int rp_id) {
		this.rp_id = rp_id;
	}
	
	
	public int getItem_id() {
		return item_id;
	}
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
}
