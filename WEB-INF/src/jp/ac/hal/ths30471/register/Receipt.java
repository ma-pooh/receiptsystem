package jp.ac.hal.ths30471.register;

import java.sql.Date;
import java.util.ArrayList;

import jp.ac.hal.papers.PaperItem;


public class Receipt {
	
	// 親レシート
	public int id;
	public Date date;
	public int Rp_id;
	
	// 子レシート
	public ArrayList<ReceiptItem> items = new ArrayList<ReceiptItem>();
	
	public void setItemID(String[] item_ids){
		items.clear();
		
		if(item_ids != null){
			for(int i = 0; i < item_ids.length; i++){
				int item_id = Integer.parseInt(item_ids[i]);
				
				ReceiptItem item = new ReceiptItem(this.id, item_id);
				items.add(item);	
			}
		}
		
	}
	
	// 合計計算用メソッド
	public int calcTotalPrice(){
		int sum = 0;
		for(ReceiptItem item : items){
			sum += item.item_price;
		}
		return sum;
	}
	

	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	public int getRp_id() {
		return Rp_id;
	}

	public void setRp_id(int rp_id) {
		Rp_id = rp_id;
	}


	
	
}


/*
// 子供レシート
private int rch_id;
private int rp_id;
private int item_id;
*/

// 先生の説明がよく理解できなかったので、お預け
// private ArrayList<Receipt2> receipt2 = new ArrayList<Receipt2>();

/*
public void setPaperItems(int[] receipt_ids, String[] item_ids){
	receipt2.clear();
	if(item_ids != null){
		for(int i = 0; i < item_ids.length; i++){
			try{
				int item_id = Integer.parseInt(item_ids[i]);
				int count = Integer.parseInt(item_counts[i]);
				PaperItem paper_item = new PaperItem(this.id, item_id, count);
				paper_items.add(paper_item);
			}
			catch(Exception ex){
			}
		}
	}
}

*/
