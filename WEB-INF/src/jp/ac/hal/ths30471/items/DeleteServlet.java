package jp.ac.hal.ths30471.items;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/product/delete")
public class DeleteServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		req.setCharacterEncoding("utf-8");
		int id = Integer.parseInt(req.getParameter("id"));

		String url = "/WEB-INF/jsp/receiptsystem/product/product.jsp";
		try {
			ItemsDAO dao = new ItemsDAO();

			dao.delete(id);

			resp.sendRedirect("/ReceiptSystem/product");
			return;

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		req.getRequestDispatcher(url).forward(req, resp);
		
		
	}
	
	

}
