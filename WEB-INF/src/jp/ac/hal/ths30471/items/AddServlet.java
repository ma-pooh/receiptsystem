package jp.ac.hal.ths30471.items;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.ac.hal.items.Item;
import jp.ac.hal.items.ItemsDao;

@WebServlet("/product/add")
public class AddServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String url = "/WEB-INF/jsp/receiptsystem/product/add.jsp";
		req.getRequestDispatcher(url).forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		req.setCharacterEncoding("utf-8");
		int id = Integer.valueOf(req.getParameter("id"));
		String item = req.getParameter("item");
		String barcode =req.getParameter("barcode");
		int price = Integer.valueOf(req.getParameter("price"));

		String url = "/WEB-INF/jsp/receiptsystem/product/product.jsp";
		try {
			ItemsDAO dao = new ItemsDAO();
			
			Product p = new Product();
			
			p.setItem_id(id);
			p.setBarcode(barcode);
			p.setItem(item);
			p.setPrice(price);
			dao.insert(p);

			// リダイレクト
			resp.sendRedirect("/ReceiptSystem/product");
			return;

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		req.getRequestDispatcher(url).forward(req, resp);
		
		
		

	}
	
	
	
	

}
