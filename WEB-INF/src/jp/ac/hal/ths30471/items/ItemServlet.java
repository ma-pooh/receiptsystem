package jp.ac.hal.ths30471.items;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/product")
public class ItemServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String url = "/WEB-INF/jsp/receiptsystem/product/product.jsp";
		
		try {
			
			ItemsDAO dao = new ItemsDAO();
			ArrayList<Product> list = null;
			list = dao.select();
			req.setAttribute("list", list);
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		req.getRequestDispatcher(url).forward(req, resp);
		

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		
		
		
		

	}

}
