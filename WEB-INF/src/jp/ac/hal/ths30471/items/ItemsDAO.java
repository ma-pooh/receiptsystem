package jp.ac.hal.ths30471.items;

import ja.ac.hal.ths30471.receiptsystem.MyDb;

import java.util.ArrayList;

import jp.ac.hal.items.Item;

public class ItemsDAO {
	
	public ItemsDAO() throws ClassNotFoundException {
		
		Class.forName("oracle.jdbc.driver.OracleDriver");
		
	}
	
	// select
	public ArrayList<Product> select(){
		
		ArrayList<Product> list = new ArrayList<Product>();
		
		try {
			
			MyDb db = new MyDb();
			
			db.prepare("SELECT item_id, barcode, item, price FROM product ORDER BY item_id");
			db.execute();
			
			while (db.next()) {
				
				Product p = new Product();
				
				p.setItem_id(db.getInt("item_id"));
				p.setBarcode(db.getString("barcode"));
				p.setItem(db.getString("item"));
				p.setPrice(db.getInt("price"));
				
				list.add(p);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	// insert
	public void insert(Product p) {
		try {
			MyDb db = new MyDb();
			
			db.prepare("INSERT INTO product(item_id, barcode, item, price) VALUES(?, ?, ?, ?)");
			db.setInt(1, p.getItem_id());
			db.setString(2, p.getBarcode());
			db.setString(3, p.getItem());
			db.setInt(4, p.getPrice());
			db.executeUpdate();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	// delete
	
	public boolean delete(int id) {
		// 成功フラグ
		boolean isSuccess = false;

		try {
			// SQL実行
			MyDb db = new MyDb();
			db.prepare("DELETE FROM product WHERE item_id = ?");
			db.setInt(1, id);
			db.executeUpdate();
			// tryの最後まできたら処理成功
			isSuccess = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		// 成否フラグを返す
		return isSuccess;
	}

}
