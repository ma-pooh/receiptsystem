<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../_head.jsp" %>

<h2>会社情報:編集</h2>

<%
	Company company = (Company)request.getAttribute("company");
%>
<form action="/ReceiptSystem/companies/edit" method="POST">
	<input type="hidden" name="id" value="<%= company.getId() %>" />
	<div class="message-fields">
		<div class="field-wrapper">
			<div class="field-label">会社名：</div>
			<div class="field-content"><input type="text" name="name" value="<%= company.getName() %>"></div>
		</div>
		<div class="field-wrapper">
			<div class="field-label">メール：</div>
			<div class="field-content"><input type="text" name="mail" value="<%= company.getMail() %>"></div>
		</div>
		<div class="field-wrapper">
			<div class="field-content">
				<input type="submit" value="登録">
			</div>
		</div>
	</div>
</form>

<%@ include file="../_foot.jsp" %>
