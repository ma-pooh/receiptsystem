<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../_head.jsp" %>

<h2>会社情報</h2>

<br/>

<div style="display: inline-block; position: relative;">

	<div style="text-align: right; margin-bottom: 8px;">
		<a href="/ReceiptSystem/companies/add">新規</a>
	</div>

	<table border="1">
	<tr>
		<th>ID</th>
		<th>会社名</th>
		<th>メールアドレス</th>
		<th><br/></th>
		<th><br/></th>
	</tr>
	<%
		ArrayList<Company> list = (ArrayList<Company>)request.getAttribute("list");
		for(Company r : list){
	%>
			<tr>
				<td><%= r.getId() %></td>
				<td><%= r.getName() %></td>
				<td><%= r.getMail() %></td>
				<td>
					<a href="/ReceiptSystem/companies/edit?id=<%= r.getId() %>">編集</a>
				</td>
				<td>
					<form action="/ReceiptSystem/companies/delete" method="POST">
					<input type="hidden" name="id" value="<%= r.getId() %>" />
					<input type="submit" value="削除" />
					</form>
				</td>
			</tr>
	<%
		}
	%>
	</table>

</div>

<%@ include file="../_foot.jsp" %>

