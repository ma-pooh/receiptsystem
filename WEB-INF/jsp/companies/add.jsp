<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../_head.jsp" %>

<h2>会社情報:新規</h2>

<form action="/ReceiptSystem/companies/add" method="POST">
	<div class="message-fields">
		<div class="field-wrapper">
			<div class="field-label">会社名：</div>
			<div class="field-content"><input type="text" name="name" /></div>
		</div>
		<div class="field-wrapper">
			<div class="field-label">メール：</div>
			<div class="field-content"><input type="text" name="mail" /></div>
		</div>
		<div class="field-wrapper">
			<div class="field-label">何か：</div>
			<div class="field-content">
				<select name="hoge">
				<option value="1">商品A</option>
				<option value="2">商品B</option>
				<option value="3">商品C</option>
				<option value="4">商品D</option>
				</select>
			</div>
		</div>
		<div class="field-wrapper">
			<div class="field-content">
				<input type="submit" value="登録">
			</div>
		</div>
	</div>
</form>

<%@ include file="../_foot.jsp" %>
