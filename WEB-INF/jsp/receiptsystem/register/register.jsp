<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../_head.jsp" %>


	
	<div class="container">

        <h1>レジ</h1>
        
        <div class="alert alert-danger" role="alert" id="error" style="display: none";>商品が見つかりません</div>
        
        <!-- 商品追加入力 (バーコード用) -->
        <div style="margin-bottom: 16px;">
				<div class="row">
				  <div class="col-lg-6">
				    <div class="input-group">
				      <input type="text" class="form-control" id="number">
				      <span class="input-group-btn">
				        <button class="btn btn-default" type="button" id="add-button">追加</button>
				      </span>
				    </div><!-- /input-group -->
				  </div><!-- /.col-lg-6 -->
				</div><!-- /.row -->
	    </div>
        
        <!-- レシート内容 -->
        <form action="/ReceiptSystem/register" method="POST">
	        <table class="table" id="receipt-table">
		        <thead>
		           <tr><th>ITEM_ID</th><th>バーコード番号</th><th>商品名</th><th>価格</th></tr>
		        </thead>
		        <tbody>

		        </tbody>
		        <tfoot style="background-color: #fee;">
		            <tr>
		            	<td>合計</td>
		            	<td id="total"></td>
		            </tr>
		        </tfoot>
	        </table>
	        <div class="input-group pull-right">
		        <input type="submit" class="btn btn-default" value="確定" />
		    </div>
	    </form>
	    
	    <!-- 商品一覧表示（実際には非表示） -->
	    <div class="row" style="color: #aaa; margin-top: 100px;" id="master-items">
	    <h2>
		    商品一覧（デバッグ用に表示）
		</h2>
      	
      	<table class="table">
			<thead>
				<tr><th>ITEM_ID</th><th>バーコード番号</th><th>商品名</th><th>価格</th></tr>
			</thead>
				<tbody>
<%
	ArrayList<Product> list = (ArrayList<Product>)request.getAttribute("list");
	for(Product p : list){
%>						
					<tr class="item-<%= p.getBarcode() %>">
						<td>
							<%= p.getItem_id() %>
							<input type="hidden" name="item_ids" value="<%= p.getItem_id() %>">
						</td>
						<td>
							<%= p.getBarcode() %>					
						</td>
						<td><%= p.getItem() %></td>
						<td class="price"><%= p.getPrice() %></td>
					</tr>
				</tbody>
<%
	}
%>
      	</table>
		</div>

    </div><!-- /.container -->



<%@ include file="../_foot.jsp" %>