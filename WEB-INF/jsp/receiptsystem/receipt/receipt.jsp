<%@page import="jp.ac.hal.ths30471.register.Receipt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../_head.jsp" %>

	<div class="container">
		
		<h1>レシート一覧</h1>
		<table class="table">
			<thead>
				<tr>
					<th>ID</th><th>日時</th><th></th>
				</tr>
			</thead>
			<tbody>
		
<%
	ArrayList<Receipt> list = (ArrayList<Receipt>)request.getAttribute("list");
	for(Receipt r : list){
%>						

			<tr>
				<td><%= r.getId() %></td>
				<td><%= r.getDate() %></td>
				<td>
					<form action="" method="post" style="display:inline-block;">
						<input type="hidden" name="id" value="<%= r.getId() %>">
						<button type="submit" class="btn-group btn btn-default">
							<span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
							明細
						</button>
					</form>					

					<form action="" method="POST" style="display:inline-block;">
						<input type="hidden" name="id" value="<%= r.getId() %>">
						<button type="submit" class="btn-group btn btn-default">
							<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
							削除
						</button>
					</form>					
				</td>				
			</tr>
			</tbody>
			
<%	
	}
%>
		</table>
		
	</div><!-- /.container -->
	
<%@ include file="../_foot.jsp" %>