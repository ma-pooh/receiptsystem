<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../_head.jsp" %>

<%
	Receipt receipt = (Receipt)request.getAttribute("receipt");
%>
<h2>概要</h2>
<table class="table">
<tr><th>ID</th><td><%= receipt.id %></td></tr>
<tr><th>日時</th><td><%= receipt.getCreatedText() %></td></tr>
</table>

<h2>明細</h2>
<table class="table">
<thead>
	<tr>
		<th>商品</th><th>金額</th>
	</tr>
</thead>
<tbody>
	<c:forEach var="item" items="${receipt.items}">
		<tr>
			<td><c:out value="${item.item_name}"/></td>
			<td><c:out value="${item.item_price}"/></td>
		</tr>
	</c:forEach>
</tbody>
<tfoot>
	<tr style="background-color: #faa;">
		<th>合計</th><th><%= receipt.calcTotalPrice() %></th>
	</tr>
</tfoot>
</table>


<%@ include file="../_foot.jsp" %>