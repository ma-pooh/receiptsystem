<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../_head.jsp" %>


<div class="container">
	<h1>商品情報：新規</h1>
<div class="row" id="content">

<form class="col-md-8 form-horizontal" role="form" action="/ReceiptSystem/product/add" method="POST">

<div class="form-group">
	<label for="code" class="col-sm-2 control-label">コード番号</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" name="id" id="id" value="" />
	</div>
</div>

<div class="form-group">
	<label for="code" class="col-sm-2 control-label">バーコード番号</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" name="barcode" id="code" value="" />
	</div>
</div>

<div class="form-group">
	<label for="name" class="col-sm-2 control-label">商品名</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" name="item" id="name" value="" />
	</div>
</div>

<div class="form-group">
	<label for="price" class="col-sm-2 control-label">単価</label>
	<div class="col-sm-10">
		<input type="text" class="form-control" name="price" id="price" value="" />
	</div>
</div>

<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
		<button type="submit" class="btn btn-default">登録!!</button>
	</div>
</div>

</form>

</div>
</div><!-- /.container -->


<%@ include file="../_foot.jsp" %>