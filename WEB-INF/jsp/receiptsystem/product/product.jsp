<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../_head.jsp" %>



	<div class="container">
        <h1>商品管理</h1>
        <div style="position: relative;" id="content">
			<div style="text-align: right; margin-bottom: 8px;">
				<a href="/ReceiptSystem/product/add" class="btn btn-default">
				<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				新規
				</a>
			</div>
		</div>
        
        <table class="table">
			<thead>
				<tr><th>コード番号</th><th>バーコド番号</th><th>商品名</th><th>価格</th></tr>
			</thead>
			<tbody>
<%
	ArrayList<Product> list = (ArrayList<Product>)request.getAttribute("list");
	for(Product p : list){
%>						
				<tr>
					<td><%= p.getItem_id() %></td>
					<td><%= p.getBarcode() %></td>
					<td><%= p.getItem() %></td>
					<td><%= p.getPrice() %></td>
					<td>
						<form action="" method="post" style="display:inline-block;">
							<input type="hidden" name="id" value="<%= p.getItem_id() %>">
							<button type="submit" class="btn-group btn btn-default">
								<span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
								編集
							</button>
						
						</form>					

						<form action="/ReceiptSystem/product/delete" method="POST" style="display:inline-block;">
							<input type="hidden" name="id" value="<%= p.getItem_id() %>">
							<button type="submit" class="btn-group btn btn-default">
								<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								削除!!
							</button>
						
						</form>					
					</td>
				</tr>
				
			</tbody>
<%
	}
%>
        </table>
    </div><!-- /.container -->
    
    

<%@ include file="../_foot.jsp" %>