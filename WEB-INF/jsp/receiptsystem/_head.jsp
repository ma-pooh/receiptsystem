<%@page import="java.util.ArrayList"%>
<%@page import="jp.ac.hal.ths30471.items.Product"%>
<%@page import="jp.ac.hal.ths30471.register.Receipt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	
	<script src="/ReceiptSystem/js/jquery.min.js"></script>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
	    <!-- Bootstrap core CSS -->
    <link href="/ReceiptSystem/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/ReceiptSystem/css/app.css" rel="stylesheet" />
    <style>
		body {
		  padding-top: 50px;
		}
		.starter-template {
		  padding: 40px 15px;
		  text-align: center;
		}
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<!-- ナビゲーション -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/ReceiptSystem/TOP">レシートシステム</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="/ReceiptSystem/product">商品管理</a></li>
            <li class="active"><a href="/ReceiptSystem/register">レジ</a></li>
            <li><a href="/ReceiptSystem/receipt">レシート一覧</a></li>
            <li><a href="">集計</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
