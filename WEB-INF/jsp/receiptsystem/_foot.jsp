<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div id="foot">
Copyright ??????
</div>

	<!-- jquery, bootstrap -->
	<script src="/ReceiptSystem/js/jquery.min.js"></script>
	<script src="/ReceiptSystem/bootstrap/js/bootstrap.min.js"></script>

	<!-- イベント処理等 -->
	<script>
		$(function(){
			$('#add-button').click(function(){
				addItem();
			});
			$("#number").keypress(function(e){
				if (e.which === 13) { // Enter
					addItem();
					return false;
				}
			});
			
			// フォーカス
			$('#number').focus();
		});
		function addItem(){
			var number = $('#number').val();
			var tr = $('#master-items .item-' + number);
			if(tr.size() != 0){
				
				// 商品追加
				tr.clone().appendTo('#receipt-table tbody');
				
				// 合計算出
				var total = 0;
				$('#receipt-table .price').each(function(){
				total += parseInt($(this).text());
				});
				$('#total').text(total);
				
				// 入力値クリア
				$('#number').val('');
			}
			else{
				// エラー表示
				$('#error').show();
			}
		}
	</script>

</body>
</html>