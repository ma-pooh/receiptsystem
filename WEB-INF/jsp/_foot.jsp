<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<div id="foot">
Copyright hogehoge
</div>

<script>
	var template;
	$(function(){
		// テンプレートをcloneしておく
		template = $('#template').clone();
		template.removeAttr('id');
		// 要素が複数ある場合にはテンプレートは消す
		if($('.item_id').size() >= 2){
			$('#template').remove();
		}
	});

	// 商品追加
	$('.item_add').click(function(){
		template.clone().appendTo('tbody');
	});

	// 商品削除
	$('body').on('click', '.item_del', function(){
		$(this).parents('tr').remove(); // 自分の親 (<TR>) を消す
	});

</script>

</body>
</html>
