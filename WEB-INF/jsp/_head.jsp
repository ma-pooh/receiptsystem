<%@page import="jp.ac.hal.companies.Company"%>
<%@page import="jp.ac.hal.items.Item"%>
<%@page import="jp.ac.hal.papers.Paper"%>
<%@page import="jp.ac.hal.papers.PaperItem"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
	<script src="/ReceiptSystem/js/jquery.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="/ReceiptSystem/css/app.css" rel="stylesheet" />
	<title>基幹システム</title>
</head>
<body>
<h1><a href="/ReceiptSystem/top">基幹システム</a></h1>

<div id="nav-links">
	<div class="nav-link">
		<a href="/ReceiptSystem/companies/index">会社情報</a>
	</div>
	<div class="nav-link">
		<a href="/ReceiptSystem/items/index">商品情報</a>
	</div>
	<div class="nav-link">
		<a href="/ReceiptSystem/papers/index">発注書情報</a>
	</div>
</div>
