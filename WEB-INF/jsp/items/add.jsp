<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../_head.jsp" %>

<h2>商品情報:新規</h2>

<form action="/ReceiptSystem/items/add" method="POST">
	<div class="message-fields">
		<div class="field-wrapper">
			<div class="field-label">商品名：</div>
			<div class="field-content"><input type="text" name="name" /></div>
		</div>
		<div class="field-wrapper">
			<div class="field-label">単価：</div>
			<div class="field-content"><input type="text" name="price" /></div>
		</div>
		<div class="field-wrapper">
			<div class="field-label">何か：</div>
			<div class="field-content">
				<select name="hoge">
				<option value="1">商品A</option>
				<option value="2">商品B</option>
				<option value="3">商品C</option>
				<option value="4">商品D</option>
				</select>
			</div>
		</div>
		<div class="field-wrapper">
			<div class="field-content">
				<input type="submit" value="登録">
			</div>
		</div>
	</div>
</form>

<%@ include file="../_foot.jsp" %>
