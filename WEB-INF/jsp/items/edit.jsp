<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../_head.jsp" %>

<h2>商品情報:編集</h2>

<%
	Item item = (Item)request.getAttribute("item");
%>
<form action="/ReceiptSystem/items/edit" method="POST">
	<input type="hidden" name="id" value="<%= item.getId() %>" />
	<div class="message-fields">
		<div class="field-wrapper">
			<div class="field-label">会社名：</div>
			<div class="field-content"><input type="text" name="name" value="<%= item.getName() %>"></div>
		</div>
		<div class="field-wrapper">
			<div class="field-label">単価：</div>
			<div class="field-content"><input type="text" name="price" value="<%= item.getPrice() %>"></div>
		</div>
		<div class="field-wrapper">
			<div class="field-content">
				<input type="submit" value="登録">
			</div>
		</div>
	</div>
</form>

<%@ include file="../_foot.jsp" %>
