<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../_head.jsp" %>

<h2>商品情報</h2>

<br/>

<div style="display: inline-block; position: relative;">

	<div style="text-align: right; margin-bottom: 8px;">
		<a href="/ReceiptSystem/items/add">新規</a>
	</div>

	<table border="1">
	<tr>
		<th>ID</th>
		<th>商品名</th>
		<th>単価</th>
		<th><br/></th>
		<th><br/></th>
	</tr>
	<%
		ArrayList<Item> list = (ArrayList<Item>)request.getAttribute("list");
		for(Item r : list){
	%>
			<tr>
				<td><%= r.getId() %></td>
				<td><%= r.getName() %></td>
				<td><%= r.getPrice() %></td>
				<td>
					<a href="/ReceiptSystem/items/edit?id=<%= r.getId() %>">編集</a>
				</td>
				<td>
					<form action="/ReceiptSystem/items/delete" method="POST">
					<input type="hidden" name="id" value="<%= r.getId() %>" />
					<input type="submit" value="削除" />
					</form>
				</td>
			</tr>
	<%
		}
	%>
	</table>

</div>

<%@ include file="../_foot.jsp" %>

