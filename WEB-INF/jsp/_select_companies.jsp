<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<%@page import="java.util.ArrayList"%>
<%@page import="jp.ac.hal.companies.Company"%>
<%@page import="jp.ac.hal.items.Item"%>
<%@page import="jp.ac.hal.papers.Paper"%>
<%@page import="jp.ac.hal.papers.PaperItem"%>

<select name="company_id">
	<%
		// パラメータ受け取り
		int company_id = -1;
		try{
			company_id = Integer.parseInt(request.getParameter("company_id"));
		}
		catch(Exception ex){
		}

		// 一覧構築
		ArrayList<Company> companies = (ArrayList<Company>)request.getAttribute("companies");
		for(Company company : companies){
	%>
			<option
				value="<%= company.getId() %>"
				<% if(company_id == company.getId()){ out.print("selected"); }  %>
				>
					<%= company.getName() %>
			</option>
	<%
		}
	%>
</select>
