<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../_head.jsp" %>

<h2>発注書情報:新規</h2>

<form action="/ReceiptSystem/papers/add" method="POST">
	<div class="message-fields">
		<div class="field-wrapper">
			<div class="field-label">発注書名：</div>
			<div class="field-content"><input type="text" name="name" /></div>
		</div>
		<div class="field-wrapper">
			<div class="field-label">発注先：</div>
			<div class="field-content">
				<jsp:include page="../_select_companies.jsp">
					<jsp:param name="company_id" value="-1" />
				</jsp:include>
				<%//@ include file="../_select_companies.jsp" %>
			</div>
		</div>
		<div class="field-wrapper">
			<div class="field-label">商品リスト：</div>
			<div class="field-content">
				<table>
					<thead>
						<tr>
							<th>商品</th><th>個数</th>
						</tr>
					</thead>
					<tbody>
						<tr id="template">
							<td>
								<jsp:include page="../_select_items.jsp">
									<jsp:param name="item_id" value="-1" />
								</jsp:include>
							</td>
							<td><input class="item_cnt" name="item_cnts" type="text" value="" style="text-align: right;"></td>
							<td><div class="item_del">×</div></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="field-wrapper">
			<div class="field-content">
				<input type="button" value="商品追加" class="item_add">
			</div>
		</div>

		<hr />

		<div class="field-wrapper">
			<div class="field-content">
				<input type="submit" value="登録">
			</div>
		</div>
	</div>
</form>

<%@ include file="../_foot.jsp" %>
