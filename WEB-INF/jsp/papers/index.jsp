<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../_head.jsp" %>

<h2>発注書情報</h2>

<br/>

<div style="display: inline-block; position: relative;">

	<div style="text-align: right; margin-bottom: 8px;">
		<a href="/ReceiptSystem/papers/add">新規</a>
	</div>

	<table border="1">
	<tr>
		<th>ID</th>
		<th>発注書名</th>
		<th><br/></th>
		<th><br/></th>
	</tr>
	<%
		ArrayList<Paper> list = (ArrayList<Paper>)request.getAttribute("list");
		for(Paper r : list){
	%>
			<tr>
				<td><%= r.getId() %></td>
				<td>
					<a href="/ReceiptSystem/papers/view?id=<%= r.getId() %>"><%= r.getName() %></a>
				</td>
				<td>
					<a href="/ReceiptSystem/papers/edit?id=<%= r.getId() %>">編集</a>
				</td>
				<td>
					<form action="/ReceiptSystem/papers/delete" method="POST">
					<input type="hidden" name="id" value="<%= r.getId() %>" />
					<input type="submit" value="削除" />
					</form>
				</td>
			</tr>
	<%
		}
	%>
	</table>

</div>

<%@ include file="../_foot.jsp" %>

