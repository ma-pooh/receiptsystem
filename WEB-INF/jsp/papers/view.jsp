<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../_head.jsp" %>

<h2>発注書情報</h2>

<%
	Paper paper = (Paper)request.getAttribute("paper");
%>
<div class="view">
	<input type="hidden" name="id" value="<%= paper.getId() %>" />
	<div class="message-fields">
		<div class="field-wrapper">
			<div class="field-label">発注書名：</div>
			<div class="field-content"><%= paper.getName() %></div>
		</div>
		<div class="field-wrapper">
			<div class="field-label">発注先：</div>
			<div class="field-content"><%= paper.getCompanyName() %></div>
		</div>
		<div class="field-wrapper">
			<div class="field-label">商品リスト：</div>
			<div class="field-content">
				<table>
					<thead>
						<tr>
							<th>商品</th><th>個数</th><th>単価</th><th>金額</th>
						</tr>
					</thead>
					<tbody>
						<%
							ArrayList<PaperItem> paperItems = paper.getPaperItems();
							for(PaperItem paperItem : paperItems){
						%>
								<tr>
									<td><%= paperItem.item_name %></td>
									<td><%= paperItem.cnt %></td>
									<td><%= paperItem.item_price %></td>
									<td><%= paperItem.calcTotalPrice() %></td>
								</tr>
						<%
							}
						%>
					</tbody>
				</table>
				<div style="margin-top:10px; background-color: #faa;">
				合計：<%= paper.calcTotalPrice() %>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="../_foot.jsp" %>
