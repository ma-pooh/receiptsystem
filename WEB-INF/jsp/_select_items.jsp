<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<%@page import="java.util.ArrayList"%>
<%@page import="jp.ac.hal.companies.Company"%>
<%@page import="jp.ac.hal.items.Item"%>
<%@page import="jp.ac.hal.papers.Paper"%>
<%@page import="jp.ac.hal.papers.PaperItem"%>

<select class="item_id" name="item_ids">
	<%
		// パラメータ受け取り
		int item_id = -1;
		try{
			item_id = Integer.parseInt(request.getParameter("item_id"));
		}
		catch(Exception ex){
		}

		// 一覧構築
		ArrayList<Item> items = (ArrayList<Item>)request.getAttribute("items");
		for(Item item : items){
	%>
			<option
				value="<%= item.getId() %>"
				<% if(item_id == item.getId()){ out.print("selected"); }  %>
				>
					<%= item.getName() %>
			</option>
	<%
		}
	%>
</select>
